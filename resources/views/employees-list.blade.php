@extends('layouts.app')

@section('content')
    <div class="container">
        <a class="nav-link" href="{{ route('home') }}">Back to dashboard</a>
        <table class="table table-bordered table-hover">
            <thead>
                <th>@sortablelink('first_name', 'First Name')</th>
                <th>@sortablelink('last_name', 'Last Name')</th>
                <th>Employee since</th>
                <th>Actions</th>
            </thead>
            <tbody>
            @if ($employees->count() == 0)
                <tr>
                    <td colspan="5">No employees to display.</td>
                </tr>
            @endif

            @foreach($employees as $employee)
                <tr>
                    <td><a href="/employees/{{ $employee->emp_id }}">{{$employee->first_name}}</a></td>
                    <td>{{$employee->last_name}}</td>
                    <td>{{$employee->date_of_joining}}</td>
                    <td>
                        <a href="/employees/{{ $employee->emp_id }}">View</a>
                        /
                        <a href="/employees/{{ $employee->emp_id }}/edit">Edit</a>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $employees->links() }}
    </div>
@endsection
