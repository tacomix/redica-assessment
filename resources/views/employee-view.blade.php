@extends('layouts.app')

@section('content')
    <div class="container">
        <a href="{{ route('employees.index') }}" class="btn btn-md btn-outline-secondary" role="button">Back to list</a>
        <a href="/employees/{{ $employee->emp_id }}/edit" class="btn btn-md btn-outline-secondary" role="button">Edit</a>
        <div class="row">
            <div class="col-md-2">
                Id
            </div>
            <div class="col-md-10">
                {{ $employee->emp_id }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                Name
            </div>
            <div class="col-md-10">
                {{ $employee->name_prefix }} {{ $employee->first_name }} {{ $employee->middle_initial }} {{ $employee->last_name }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                SSN
            </div>
            <div class="col-md-10">
                {{ $employee->ssn }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                Gender
            </div>
            <div class="col-md-10">
                {{ $employee->gender }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                Parents
            </div>
            <div class="col-md-10">
                {{ $employee->father_name }} & {{ $employee->mother_name }} (nee {{ $employee->mother_maidens_name }})
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                Date of Birth
            </div>
            <div class="col-md-10">
                {{ $employee->date_of_birth }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                Date of Joining
            </div>
            <div class="col-md-10">
                {{ $employee->date_of_joining }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                Salary
            </div>
            <div class="col-md-10">
                {{ $employee->salary }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                Phone number
            </div>
            <div class="col-md-10">
                {{ $employee->phone_number }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                Address
            </div>
            <div class="col-md-10">
                {{ $employee->city }}, {{ $employee->state }} {{ $employee->zip }}
            </div>
        </div>
    </div>
@endsection
