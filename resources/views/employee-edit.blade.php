@extends('layouts.app')

@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{ route('employees.update', [$employee->emp_id]) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="row">
                <button type="submit" class="btn btn-md btn-primary">Submit</button>
                <a href="{{ route('employees.index') }}" class="btn btn-md btn-outline-secondary" role="button">Cancel</a>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="name_prefix">Name Prefix</label>
                        <input class="form-control" name="name_prefix" value="{{ old('name_prefix', $employee->name_prefix) }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                        <input class="form-control" name="first_name" value="{{ old('first_name', $employee->first_name) }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="middle_initial">Middle Initial</label>
                        <input class="form-control" name="middle_initial" value="{{ old('middle_initial', $employee->middle_initial) }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="last_name">Last Name</label>
                        <input class="form-control" name="last_name" value="{{ old('last_name', $employee->last_name) }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="gender">Gender</label>
                        <select class ="form-control" name="gender">
                            <option value="M" {{ old('gender', $employee->gender) == 'M' ? 'selected' : '' }}>M</option>
                            <option value="F" {{ old('gender', $employee->gender) == 'F' ? 'selected' : '' }}>F</option>
                            <option value="O" {{ old('gender', $employee->gender) == 'O' ? 'selected' : '' }}>Other</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input class="form-control" name="email" value="{{ old('email', $employee->email) }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="father_name">Father Name</label>
                        <input class="form-control" name="father_name" value="{{ old('father_name', $employee->father_name) }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="mother_name">Mother Name</label>
                        <input class="form-control" name="mother_name" value="{{ old('mother_name', $employee->mother_name) }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="mother_maidens_name">Mother's Maiden Name</label>
                        <input class="form-control" name="mother_maidens_name" value="{{ old('mother_maidens_name', $employee->mother_maidens_name) }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="date_of_birth">Date of Birth (YYYY-MM-DD)</label>
                        <input class="form-control" name="date_of_birth" value="{{ old('date_of_birth', $employee->date_of_birth) }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="date_of_joining">Date of Joining (YYYY-MM-DD)</label>
                        <input class="form-control" name="date_of_joining" value="{{ old('date_of_joining', $employee->date_of_joining) }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="salary">Salary</label>
                        <input class="form-control" name="salary" value="{{ old('salary', $employee->salary) }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="ssn">SSN</label>
                        <input class="form-control" name="ssn" value="{{ old('ssn', $employee->ssn) }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="phone_number">Phone number</label>
                        <input class="form-control" name="phone_number" value="{{ old('phone_number', $employee->phone_number) }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="city">City</label>
                        <input class="form-control" name="city" value="{{ old('city', $employee->city) }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="state">State</label>
                        <input class="form-control" name="state" value="{{ old('state', $employee->state) }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="zip">Zip</label>
                        <input class="form-control" name="zip" value="{{ old('zip', $employee->zip) }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <button type="submit" class="btn btn-md btn-primary">Submit</button>
                <a href="{{ route('employees.index') }}" class="btn btn-md btn-outline-secondary" role="button">Cancel</a>
            </div>
        </form>
    </div>
@endsection
