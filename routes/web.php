<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'employees'], function() {
    Route::get('/', 'EmployeeController@index')->name('employees.index');
    Route::get('/{emp_id}', 'EmployeeController@show')->name('employees.show');
    Route::get('/{emp_id}/edit', 'EmployeeController@edit')->name('employees.edit');
    Route::put('/{emp_id}', 'EmployeeController@update')->name('employees.update');
});
