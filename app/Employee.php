<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Employee extends Model
{
    use Sortable;

    protected $primaryKey = 'emp_id';
    public $sortable = ['first_name', 'last_name'];
    protected $fillable = [
        'name_prefix',
        'first_name',
        'middle_initial',
        'last_name',
        'gender',
        'email',
        'father_name',
        'mother_name',
        'mother_maidens_name',
        'date_of_birth',
        'date_of_joining',
        'salary',
        'ssn',
        'phone_number',
        'city',
        'state',
        'zip',
    ];
}
