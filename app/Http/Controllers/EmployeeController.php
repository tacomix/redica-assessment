<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getAllEmployers()
    {
        $employees = Employee::get()->toJson(JSON_PRETTY_PRINT);
        return response($employees, 200);
    }

    public function index()
    {
        $employees = Employee::sortable('last_name')->paginate(10);
        return view('employees-list')->with('employees', $employees);;
    }

    public function show($emp_id)
    {
        $employee = Employee::find($emp_id);
        return view('employee-view', compact('employee'));
    }

    public function edit($emp_id)
    {
        $employee = Employee::find($emp_id);
        return view('employee-edit', compact('employee'));
    }

    public function update(Request $request, $emp_id)
    {
        $request->validate([
            'middle_initial' => 'max:1',
            'email' => 'email',
            'date_of_birth' => 'date_format:Y-m-d',
            'date_of_joining' => 'date_format:Y-m-d',
            'salary' => 'numeric',
        ]);

        $employee = Employee::find($emp_id);
        $employee->update($request->all());
        return redirect('employees')->with('success', 'Successfully updated employee');
    }
}
