<?php

namespace App\Http\Controllers\API;

use App\Employee;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function getEmployeesList() {
        $data = Employee::sortable('last_name')->paginate(10);
        return response()->json($data,200);
    }
}
