<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = file('/var/www/database/seeds/employee_data.csv');
        foreach ($csvFile as $line) {
            $data = str_getcsv($line);
            if ($data[0] && $data[0] != 'Emp ID') {
                $dob = strtotime($data[10]);
                $doj = strtotime($data[11]);

                DB::table('employees')->insert([
                    'emp_id' => $data[0],
                    'name_prefix' => $data[1] ?? null,
                    'first_name' => $data[2] ?? null,
                    'middle_initial' => $data[3] ?? null,
                    'last_name' => $data[4] ?? null,
                    'gender' => $data[5] ?? null,
                    'email' => $data[6] ?? null,
                    'father_name' => $data[7] ?? null,
                    'mother_name' => $data[8] ?? null,
                    'mother_maidens_name' => $data[9] ?? null,
                    'date_of_birth' => $dob ? date('Y-m-d', $dob) : null,
                    'date_of_joining' => $doj ? date('Y-m-d', $doj) : null,
                    'salary' => intval($data[12]) ?? null,
                    'ssn' => $data[13] ?? null,
                    'phone_number' => $data[14] ?? null,
                    'city' => $data[15] ?? null,
                    'state' => $data[16] ?? null,
                    'zip' => $data[17] ?? null,
                ]);
            }
        }
    }
}
