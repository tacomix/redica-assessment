<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('emp_id');
            $table->string('name_prefix')->nullable(true);
            $table->string('first_name')->nullable(true);
            $table->string('middle_initial', 1)->nullable(true);
            $table->string('last_name')->nullable(true);
            $table->string('gender', 1)->nullable(true);
            $table->string('email')->nullable(true);
            $table->string('father_name')->nullable(true);
            $table->string('mother_name')->nullable(true);
            $table->string('mother_maidens_name')->nullable(true);
            $table->date('date_of_birth')->nullable(true);
            $table->date('date_of_joining')->nullable(true);
            $table->integer('salary')->nullable(true);
            $table->string('ssn', 11)->nullable(true);
            $table->string('phone_number', 20)->nullable(true);
            $table->string('city')->nullable(true);
            $table->string('state')->nullable(true);
            $table->string('zip', 10)->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
